package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.receiver;

import pt.inesc.termite.wifidirect.SimWifiP2pBroadcast;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Joao on 16/05/2018.
 */

public class SimWifiP2pReceiver extends BroadcastReceiver {

  private WifiService service;

  public SimWifiP2pReceiver(WifiService service) {
    this.service = service;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    if (intent.getAction() == null) {
      return;
    }

    switch (intent.getAction()) {
      case (SimWifiP2pBroadcast.WIFI_P2P_PEERS_CHANGED_ACTION):
        service.updatePeers();
        break;
      case (SimWifiP2pBroadcast.WIFI_P2P_NETWORK_MEMBERSHIP_CHANGED_ACTION):
        service.updateNetwork();
        break;
    }
  }
}
