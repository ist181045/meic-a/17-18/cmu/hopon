package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

public class User {

  private transient static User instance;
  private String sessionId;
  private transient String username;

  public User(String sessionId) {
    this.sessionId = sessionId;
    instance = this;
  }

  public User(String username, String sessionId) {

    this.sessionId = sessionId;
    this.username = username;
    instance = this;
  }

  //TODO: Change this
  public static User getInstance() {
    if (instance == null) {
      throw new RuntimeException("No User Exception");
    }
    return instance;
  }

  public static void destroy() {
    instance = null;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
