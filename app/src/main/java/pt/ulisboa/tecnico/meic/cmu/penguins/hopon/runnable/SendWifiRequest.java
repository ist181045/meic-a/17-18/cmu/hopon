package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.runnable;

import java.io.IOException;
import java.util.Locale;

import com.squareup.moshi.Moshi;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocket;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.SimWifiRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Created by Joao on 16/05/2018.
 */

public class SendWifiRequest extends AsyncTask<Void, Void, Void> {

  private static final String TAG = "SendWifiRequest";

  private String hostname;
  private int port;
  private String content;

  public SendWifiRequest(String hostname, int port, String content) {
    this.hostname = hostname;
    this.port = port;
    this.content = content;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    Log.d(TAG, String.format(Locale.getDefault(), "Attempting to contact %s", hostname));
    try {
      SimWifiRequest request = new SimWifiRequest("share", content, WifiService.getDeviceName());
      SimWifiP2pSocket socket = new SimWifiP2pSocket(hostname, port);
      socket.getOutputStream().write(
          (new Moshi.Builder().build().adapter(SimWifiRequest.class).toJson(request) + "\n")
              .getBytes());
      socket.getOutputStream().close();
      socket.close();
    } catch (IOException e) {
      //TODO: handle this
      e.printStackTrace();
    }
    return null;
  }
}