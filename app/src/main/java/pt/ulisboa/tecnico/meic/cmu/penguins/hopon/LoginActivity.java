package pt.ulisboa.tecnico.meic.cmu.penguins.hopon;

import java.io.IOException;

import com.squareup.moshi.Moshi;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.AuthRequest;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util.Util.hideKeyboard;
import static pt.ulisboa.tecnico.meic.cmu.penguins.hopon.util.Util.showKeyboard;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

  private final static String TAG = "LoginActivity";

  // UI references.
  private EditText username;
  private EditText ticketCode;
  private View progressView;
  private View loginFormView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    // Set up the login form.
    username = findViewById(R.id.username);
    ticketCode = findViewById(R.id.ticket_code);

    Button loginButton = findViewById(R.id.login_button);
    loginButton.setOnClickListener(this::attemptAuth);
    Button signUpButton = findViewById(R.id.sign_up_button);
    signUpButton.setOnClickListener(this::attemptAuth);

    ticketCode.setOnEditorActionListener((textView, id, keyEvent) -> {
      if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
        attemptAuth(loginButton);
        return true;
      }
      return false;
    });

    loginFormView = findViewById(R.id.login_form);
    progressView = findViewById(R.id.login_progress);

    HopOnApi.init(this);

  }

  private void attemptAuth(View v) {
    // Reset errors. TODO: Move this to onChange
    username.setError(null);
    ticketCode.setError(null);

    // Store values at the time of the login attempt.
    String usernameText = username.getText().toString().trim();
    String ticketCodeText = ticketCode.getText().toString();

    boolean cancel = false;
    View focusView = null;

    if (TextUtils.isEmpty(usernameText) || !isUsernameValid(usernameText)) {
      username.setError(getString(R.string.error_invalid_username));
      focusView = username;
      cancel = true;
    } else if (TextUtils.isEmpty(ticketCodeText) || !isTicketCodeValid(ticketCodeText)) {
      ticketCode.setError(getString(R.string.error_invalid_ticket_code));
      focusView = ticketCode;
      cancel = true;
    }

    if (cancel) {
      focusView.requestFocus();
    } else {
      hideKeyboard(this);
      showProgress(true);
      startAuth(usernameText, ticketCodeText, v.getId());
    }
  }

  private void startAuth(String usernameText, String ticketCodeText, @IdRes int buttonId) {
    AuthRequest authRequest = new AuthRequest(usernameText, ticketCodeText);
    final Context context = this;
    Callback<User> callback =
        new Callback<User>() {
          @Override
          public void onResponse(@NonNull Call<User> call, Response<User> response) {
            if (!response.isSuccessful()) {
              if (response.code() == 418) {
                setError(username, response.errorBody());
              } else {
                setError(ticketCode, response.errorBody());
              }
              resetForm(context);

            } else {

              User user = response.body();
              if (user == null) {
                Log.e(TAG, "Server is going crazy. Blame backend");
                setError(ticketCode, null);
                resetForm(context);
              } else {
                HopOnApi.getInstance().setAuth(context, user.getSessionId());
                startMainActivity(new User(username.getText().toString(), user.getSessionId()));
              }
            }
          }

          @Override
          public void onFailure(@NonNull Call<User> call, Throwable t) {
            t.printStackTrace();
            setError(ticketCode, null);
            resetForm(context);
          }
        };
    if (buttonId == R.id.login_button) {

      HopOnApi.getInstance().getService().login(authRequest).enqueue(callback);
    } else if (buttonId == R.id.sign_up_button) {
      HopOnApi.getInstance().getService().registerUser(authRequest).enqueue(callback);
    }
  }

  private void setError(EditText editText, @Nullable ResponseBody errors) {
    try {
      if (errors == null) {
        throw new AssertionError("Error Body is null");
      }
      editText.setError(errors.string());
    } catch (IOException | AssertionError e) {
      editText.setError("Unable to connect to server");
    } finally {
      editText.requestFocus();
    }
  }

  private void resetForm(Context c) {
    ticketCode.setText("");
    showKeyboard(c, ticketCode);
    showProgress(false);
  }

  private boolean isUsernameValid(String username) {
    return username.length() >= 8;
  }

  private boolean isTicketCodeValid(String ticketCode) {
    return true;
  }

  /**
   * Shows the progress UI and hides the login form.
   */
  private void showProgress(final boolean show) {
    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
    loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);

  }


  private void startMainActivity(User user) {
    Intent intent = new Intent(this, LandmarkListActivity.class);
    String message = username.getText().toString();
    intent.putExtra(EXTRA_MESSAGE, message);
    intent.putExtra("user", new Moshi.Builder().build().adapter(User.class).toJson(user));
    startActivity(intent);
  }
}

