package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

public class AuthRequest {

  private String username;
  private String ticketCode;


  public AuthRequest(String username, String ticketCode) {
    this.username = username;
    this.ticketCode = ticketCode;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getTicketCode() {
    return ticketCode;
  }

  public void setTicketCode(String ticketCode) {
    this.ticketCode = ticketCode;
  }
}
