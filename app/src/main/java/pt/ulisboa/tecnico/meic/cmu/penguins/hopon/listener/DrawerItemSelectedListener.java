package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.listener;

import com.squareup.moshi.Moshi;
import java.lang.ref.WeakReference;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.BaseActivity;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.LandmarkListActivity;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.R;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.RankingListActivity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.User;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;

public class DrawerItemSelectedListener implements NavigationView.OnNavigationItemSelectedListener {

  private WeakReference<DrawerLayout> drawerLayoutWeakReference;
  private WeakReference<Context> actualActivity;


  public DrawerItemSelectedListener(DrawerLayout drawerLayout, Context context) {
    this.drawerLayoutWeakReference = new WeakReference<>(drawerLayout);
    this.actualActivity = new WeakReference<>(context);
  }


  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();
    Class toActivity;
    if (id == R.id.nav_landmarks) {
      // Handle the camera action
      toActivity = LandmarkListActivity.class;
    } else if (id == R.id.nav_gallery) {
      // Handle the camera action
      toActivity = RankingListActivity.class;
    } else {
      return false;
    }
    if (actualActivity.get().getClass().equals(toActivity)) {
      drawerLayoutWeakReference.get().closeDrawer(GravityCompat.START);
      return false;
    } else {
      Intent intent = new Intent(actualActivity.get(), toActivity);
      ((BaseActivity) actualActivity.get()).setLogout(false);
      intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP);
      actualActivity.get().startActivity(intent);
      return false;
    }

  }
}
