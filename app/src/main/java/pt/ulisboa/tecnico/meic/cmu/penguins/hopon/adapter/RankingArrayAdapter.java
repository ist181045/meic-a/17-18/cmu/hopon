package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter;

import java.util.Date;
import java.util.Locale;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.R;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.RankingElement;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RankingArrayAdapter extends ArrayAdapter<RankingElement> {

  public RankingArrayAdapter(@NonNull Context context,
      int resource) {
    super(context, resource);
  }

  @Override
  public @NonNull
  View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    RankingElement rankingElement = getItem(position);
    ViewHolder viewHolder;

    if (convertView == null) {
      viewHolder = new ViewHolder();
      convertView = LayoutInflater
          .from(getContext()).inflate(R.layout.ranking_list_element, parent, false);
      viewHolder.positionView = convertView.findViewById(R.id.ranking_position);
      viewHolder.username = convertView.findViewById(R.id.ranking_username);
      viewHolder.points = convertView.findViewById(R.id.ranking_points);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }
    if (rankingElement != null) {
      viewHolder.positionView.setText(String.format(Locale.getDefault(), "%d", (position + 1)));
      viewHolder.username.setText(rankingElement.getUsername());
      Date date = new Date(rankingElement.getTime());
      viewHolder.points
          .setText(String.format(Locale.getDefault(), "Score: %d   Time %tM:%tS", rankingElement.getPoints(), date, date));
    }

    return convertView;
  }

  private static class ViewHolder {

    TextView positionView;
    TextView username;
    TextView points;
  }
}
