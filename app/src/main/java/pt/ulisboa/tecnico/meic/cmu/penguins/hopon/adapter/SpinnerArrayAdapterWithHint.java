package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Answer;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SpinnerArrayAdapterWithHint extends ArrayAdapter<Answer> {

  public SpinnerArrayAdapterWithHint(
      @NonNull Context context,
      int resource) {
    super(context, resource);
  }

  @Override
  public boolean isEnabled(int position) {
    return position != 0;
  }

  @Override
  public View getDropDownView(int position, View convertView,
      @NonNull ViewGroup parent) {

    View view = super.getDropDownView(position, convertView, parent);
    TextView tv = (TextView) view;
    tv.setText(getItem(position).getText());
    if (position == 0) {
      // Set the hint text color gray
      tv.setTextColor(Color.GRAY);
    } else {
      tv.setTextColor(Color.BLACK);
    }
    return view;
  }
}
