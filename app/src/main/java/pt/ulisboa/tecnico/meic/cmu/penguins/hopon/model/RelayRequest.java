package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model;

public class RelayRequest {

  private String hmac;
  private String token;

  public String getIv() {
    return iv;
  }

  public void setIv(String iv) {
    this.iv = iv;
  }

  private String iv;
  private String key;
  private String content;

  public RelayRequest() {
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getToken() {
    return token;
  }

  public String getHmac() {
    return hmac;
  }

  public void setHmac(String hmac) {
    this.hmac = hmac;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
