package pt.ulisboa.tecnico.meic.cmu.penguins.hopon;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import java.util.ArrayList;
import java.util.List;

import pt.inesc.termite.wifidirect.sockets.SimWifiP2pSocketManager;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.adapter.LandmarkListAdapter;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.listener.DrawerItemSelectedListener;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.Landmark;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LandmarkListActivity extends BaseActivity {

  public static SparseArray<Landmark> landmarks = new SparseArray<>();

  private final static String TAG = "LandmarkListActivity";
  private final LandmarkListAdapter landmarkAdapter = new LandmarkListAdapter();

  private SwipeRefreshLayout swipeLayout;
  private RecyclerView recyclerView;

  public static SparseArray<Landmark> getLandmarks() {
    return landmarks;
  }

  public static void deleteLandmarks() {
    landmarks = new SparseArray<>();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    SimWifiP2pSocketManager.Init(getApplicationContext());
    startService(new Intent(getApplicationContext(), WifiService.class));

    /* Setup ui elements */
    setContentView(R.layout.activity_landmark_list);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(new DrawerItemSelectedListener(drawer, this));

    // Set Landmark menu item checked
    navigationView.getMenu().getItem(0).setChecked(true);

    // Init swipeToRefreshLayout
    swipeLayout = findViewById(R.id.simpleSwipeRefreshLayout);
    swipeLayout.setOnRefreshListener(this::handleLandmarks);
    swipeLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
        getResources().getColor(R.color.colorAccent),
        getResources().getColor(R.color.colorPrimaryDark));

    /* Initialize recyclerView and add landmarks from the server */
    recyclerView = findViewById(R.id.view_landmark_list);
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(landmarkAdapter);

    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
        recyclerView.getContext(), layoutManager.getOrientation());
    recyclerView.addItemDecoration(dividerItemDecoration);


  }

  @Override
  protected void onResume() {
    super.onResume();
    swipeLayout.setRefreshing(true);
    handleLandmarks();
  }

  private void handleLandmarks() {
    if (LandmarkListActivity.landmarks.size() == 0) {
      HopOnApi.getInstance().getService().getLandmarks().enqueue(new Callback<List<Landmark>>() {
        @Override
        public void onResponse(@NonNull Call<List<Landmark>> call,
            @NonNull Response<List<Landmark>> response) {
          if (response.isSuccessful()) {
            List<Landmark> body = response.body();
            assert body != null;

            for (Landmark landmark : body) {
              LandmarkListActivity.landmarks.put(landmark.getId(), landmark);
            }
            landmarkAdapter.submitList(body);
            swipeLayout.setRefreshing(false);

          }
        }

        @Override
        public void onFailure(@NonNull Call<List<Landmark>> call, @NonNull Throwable t) {
          Toast
              .makeText(LandmarkListActivity.this, "Unable to connect to server", Toast.LENGTH_LONG)
              .show();
          swipeLayout.setRefreshing(false);

        }
      });
    }
    List<Landmark> landmarkCollector = new ArrayList<>(landmarks.size());
    for (int i = 0; i < landmarks.size(); i++) {
      Landmark landmark = landmarks.valueAt(i);
      landmarkCollector.add(landmark);
      if (landmark.isAnswered()) {
        CircleImageView imageView = recyclerView.getChildAt(i)
            .findViewById(R.id.ranking_position);
        if (imageView.getBorderWidth() == 0) {
          imageView.setBorderWidth(5);
          imageView.setBorderColor(
              ContextCompat.getColor(imageView.getContext(), R.color.colorAccent));
        }
      }
    }
    landmarkAdapter.submitList(landmarkCollector);
    swipeLayout.setRefreshing(false);

  }

  @Override
  public void onSaveInstanceState(Bundle savedInstanceState) {
    super.onSaveInstanceState(savedInstanceState);
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }
}
