package pt.ulisboa.tecnico.meic.cmu.penguins.hopon;

import java.io.IOException;
import java.util.Locale;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.model.User;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.HopOnApi;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service.WifiService;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity {

  protected User user;
  private boolean logout = true;

  public void setLogout(boolean logout) {
    this.logout = logout;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    try {
      this.user = User.getInstance();
    } catch (Exception e1) {
      Intent intent = getIntent();
      String user = intent.getStringExtra("user");
      JsonAdapter<User> userJsonAdapter = new Moshi.Builder().build().adapter(User.class);
      try {
        this.user = userJsonAdapter.fromJson(user);
      } catch (IOException e) {
        executeLogout(true);
      }
    }
  }

  @Override
  public void onBackPressed() {
    logout = false;
    super.onBackPressed();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.settings_menu, menu);
    return true;
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if(logout)
      executeLogout(false);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.action_settings:
        break;
      case R.id.action_logout:
        executeLogout(true);
      default:
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  public void executeLogout(boolean restore) {

    User.destroy();
    LandmarkListActivity.deleteLandmarks();

    //Invalidate sessionId in server
    try {
      HopOnApi.getInstance().getService().logout().enqueue(new Callback<Void>() {
        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {
          //do nothing
        }

        @Override
        public void onFailure(Call<Void> call, Throwable t) {
          //do nothing
        }
      });
      HopOnApi.getInstance().setAuth(this, null);

    }
    catch (RuntimeException ignored) {}

    stopService(new Intent(getApplicationContext(), WifiService.class));

    if(restore) {
      Intent intent = new Intent(this, LoginActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(intent);
    }

  }


}
