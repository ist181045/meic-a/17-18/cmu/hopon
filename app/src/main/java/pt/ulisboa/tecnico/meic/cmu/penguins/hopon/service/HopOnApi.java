package pt.ulisboa.tecnico.meic.cmu.penguins.hopon.service;

import android.content.Context;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.R;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.interceptor.AuthInterceptor;
import pt.ulisboa.tecnico.meic.cmu.penguins.hopon.interceptor.RelayInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class HopOnApi {

  private static HopOnApi instance;
  private final static String SERVER_URL = "https://194.210.230.167:44343";
  //private final static String SERVER_URL = "https://4bfbad22-0939-4a36-b28d-8837a520a607.mock.pstmn.io";

  private HopOnService service;

  private static Retrofit baseInstance;
  private static OkHttpClient baseClient;
  private static X509Certificate cert;

  private HopOnApi(Context c) {
    /* Retrofit instantiation */
    baseInstance = new Retrofit.Builder()
        .baseUrl(SERVER_URL)
        .client(createOkHttpClient(c, null))
        .addConverterFactory(MoshiConverterFactory.create())
        .build();
    service = baseInstance.create(HopOnService.class);

  }

  public static X509Certificate getCert() {
    return cert;
  }

  public static HopOnApi getInstance() {
    if (instance == null) {
      throw new RuntimeException("U HAV TO INIT");
    }
    return instance;
  }

  public static void init(Context ctx) {
    if (instance == null) {
      instance = new HopOnApi(ctx);
    }
  }

  private OkHttpClient createOkHttpClient(Context c, String token) {
    if (baseClient != null) {
      for (Interceptor interceptor : baseClient.interceptors()) {
        if (interceptor instanceof AuthInterceptor && token == null) {
          Builder newBuilder = baseClient.newBuilder();
          newBuilder.interceptors().remove(interceptor);
          for (Interceptor interceptor1 : baseClient.interceptors()) {
            if(interceptor1 instanceof RelayInterceptor) {
              newBuilder.interceptors().remove(interceptor1);
              break;
            }
          }
          return baseClient = newBuilder.build();
        }
      }
      if (token != null) {
        baseClient = baseClient.newBuilder().addInterceptor(new AuthInterceptor(token)).addInterceptor(new RelayInterceptor()).build();
      }
      return baseClient;
    }

    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    loggingInterceptor.setLevel(Level.BODY);


    /* OkHttpClient */
    try (InputStream is = c.getResources().openRawResource(R.raw.server)) {
      CertificateFactory cf = CertificateFactory.getInstance("X.509");
      cert = (X509Certificate) cf.generateCertificate(is);

      KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
      keyStore.load(null, null);
      keyStore.setCertificateEntry("server", cert);

      // Create a TrustManager that trusts the CAs in our KeyStore.
      String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
      TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm);
      trustManagerFactory.init(keyStore);

      TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

      // Create an SSLSocketFactory that uses our TrustManager
      SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
      sslContext.init(null, trustManagers, null);
      Builder builder = new Builder()
          .addInterceptor(loggingInterceptor)
          .sslSocketFactory(sslContext.getSocketFactory(), ((X509TrustManager) trustManagers[0]))
          .hostnameVerifier(((hostname, session) -> true));

      baseClient = builder.build();
      if (token != null) {
        baseClient = baseClient.newBuilder().addInterceptor(new AuthInterceptor(token)).addInterceptor(new RelayInterceptor()).build();
      }
      return baseClient;

    } catch (Throwable pokemon) {
      //TODO: Handle dis
      throw new RuntimeException(pokemon);

    }
  }


  public void setAuth(Context ctx, String token) {
    service = baseInstance.newBuilder()
        .client(createOkHttpClient(ctx, token)).build().create(HopOnService.class);
  }

  public HopOnService getService() {
    return service;
  }
}
